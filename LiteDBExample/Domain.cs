﻿namespace LiteDBExample
{
    public class Company
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public List<User> Users { get; set; }
    }
    public class User
    {
        public string Name { get; set; }
        public string Position { get; set; }
    }
}
